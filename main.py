import os
import sys
import shutil
import pwinput
import logging
import requests
import argparse
import coloredlogs
from bs4 import BeautifulSoup
from requests.auth import HTTPBasicAuth


log = logging.getLogger("password")
LOGLEVEL = os.environ.get('LOG_LEVEL', 'INFO').upper()
logging.basicConfig(
    format='%(asctime)s %(levelname)-8s %(message)s',
    level=LOGLEVEL,
    datefmt='%Y-%m-%d %H:%M:%S',
    handlers=[logging.StreamHandler(sys.stdout)])
coloredlogs.install(isatty=True, level=LOGLEVEL)


def _parse_args():
    parser = argparse.ArgumentParser(
        description='Pull images')
    parser.add_argument(
        "--url", '-u', default="gitlab.com", required=True, type=str, help="url of web page")
    parser.add_argument(
        '--dir_path', '-d', required=True, type=str, help="Output directory path")
    parser.add_argument(
        '--username', '-user', required=False, type=str, help="Username")
    args = parser.parse_args()
    return args


def ask_pass():
    try:
        while True:
            password = pwinput.pwinput(prompt='Password: ')
            password1 = pwinput.pwinput(prompt='Repeat Password: ')
            if password != password1:
                log.fatal("Passwords are not matching please repeat")
            elif password == password1:
                return password
    except Exception as e:
        log.fatal(f"not able to get password: {e}")


def main():
    args = _parse_args()
    r = ""
    if args.username:
        r = requests.get(f"https://{args.url}", auth=HTTPBasicAuth(args.username, ask_pass()))
    else:
        r = requests.get(f"https://{args.url}")

    soup = BeautifulSoup(r.content, "html.parser")
    thumbnail_elements = soup.find_all("img")
    print(thumbnail_elements)
    for element in thumbnail_elements:
        log.debug(element['src'])
        filename = element['src'].split("/")[-1]
        log.debug(filename)

        res = requests.get(f"https://{args.url}{element['src']}", stream=True)

        if res.status_code == 200:
            with open(filename, 'wb') as f:
                shutil.copyfileobj(res.raw, f)
            log.info('Image sucessfully Downloaded: ', filename)
        else:
            log.fatal('Image Couldn\'t be retrieved')


if __name__ == '__main__':
    main()
