# python-image-pull



## Getting started

Script to pull images from provided url, also capability to log in to web page

```
pip install -r requirements.txt
```

## Options are the following:

```
  -h, --help            show this help message and exit
  --url URL, -u URL     url of web page
  --dir_path DIR_PATH, -d DIR_PATH
                        Output directory path
  --username USERNAME, -user USERNAME
                        Username
```

## run from shell

```
python3 main.py -u URL -d /Path/to/be/saved
```
## With outhentication:

```
python3 main.py -u URL -d /Path/to/be/saved -user Username

```
